import logging
from urllib.request import urlopen
from datetime import timedelta

from celery.task.base import task
from django.conf import settings
from django.contrib.auth.models import User
from django.db.transaction import atomic
from django.utils.http import urlencode
from django.utils.timezone import now

from .models import PasswordChange


logger = logging.getLogger('django.request')


@task(max_retries=50, default_retry_delay=2 * 60)
def send_password_change_notifications(email=None):
    if email is not None:
        objects = PasswordChange.objects.filter(saved=True)\
                                        .values_list('email', flat=True)
        objects = User.objects.filter(email__in=objects)
    elif PasswordChange.objects.filter(saved=True, email=email).exists():
        objects = User.objects.filter(email=email)
    else:
        return

    objects = objects.extra(select={
        'mail': "email", 'hash': "password", 'c': "'pass'"})
    for params in objects.values('mail', 'hash', 'c'):
        url = settings.PASSWORD_CHANGE_NOTIFICATION_URL + "?" + urlencode(params)
        u = urlopen(url)
        resp = u.read().decode('utf-8')
        u.close()
        if resp == 'OK':
            with atomic():
                PasswordChange.objects.filter(email=params['mail']).delete()

    ttl = now() - timedelta(1)
    PasswordChange.objects.filter(saved=False, added__lte=ttl).delete()
