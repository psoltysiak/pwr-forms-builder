from rest_framework import serializers
from rest_framework import status
from rest_framework.exceptions import APIException

from django.db.utils import IntegrityError
from django.utils.translation import ugettext as _

from django.contrib.auth.models import User
from auth_ex.models import UserProfile

import datetime


class DuplicationError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_code = 'field_duplicated'
    default_detail = 'Field is duplicated.'

    def __init__(self, detail, field, status_code=status_code, error_code='duplicated_field'):
        self.status_code = status_code
        self.detail = {'detail': detail, 'error_code': error_code, 'field': field}


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email')
        write_only_fields = ('password',)
        read_only_fields = ('is_staff', 'is_superuser', 'is_active', 'date_joined',)
        extra_kwargs = {
            'username': {
                'validators': [],
            }
        }


class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = (
            'id',
            'user',
            'customer_id',
            'first_name',
            'last_name',
            'business',
            'nip',
            'city',
            'street',
            'local_number',
            'zip_code',
            'birthday',
            'phone',
            'registration_source',
        )

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        if User.objects.filter(username=user_data['username']):
            raise DuplicationError(_('User with that username actually exists.'), 
                                   'username', 
                                   status_code=status.HTTP_409_CONFLICT, 
                                   error_code='duplicated_username')
        else:
            user = User.objects.create_user(last_login=datetime.datetime.now(), 
                                            **user_data)
            user.save() 
            user_profile = UserProfile.objects.filter(user_id=user.id)
            user_profile.update(**validated_data)
            return user_profile[0]
