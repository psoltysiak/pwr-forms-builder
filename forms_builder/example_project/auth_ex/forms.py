from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.utils.translation import ugettext as _
from registration.forms import (RegistrationFormTermsOfService,
                                RegistrationFormUniqueEmail)
from registration.models import RegistrationProfile

from goods.forms import GoodsFormSet, UserGoodCategoryFormSet
from utils.forms import FormContainer
from utils.shortcuts import template_email

from .models import UserProfile, User


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label=_('Login / E-mail'), 
        max_length=75,
    )
    remember_me = forms.BooleanField(
        label=_('Remember me'),
        initial=False,
        required=False,
    )

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            # TODO: Remove validation when will be resolve issue with duplicated user emails
            User.objects.get(email__iexact=username)
        except User.MultipleObjectsReturned:
            raise forms.ValidationError(
                _('Please, use login instead of email address.')
            )
        except User.DoesNotExist:
            try:
                User.objects.get(username__iexact=username)
            except User.DoesNotExist:
                raise forms.ValidationError(
                    _('You have entered an incorrect login and password or the '
                      'account does not exist.')
                )
            except User.MultipleObjectsReturned:
                raise forms.ValidationError(
                    _('Please, try to use email address instead of login.')
                )
        return username


class ProfileEditForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ('user', 'customer_id', 'card_id', 'registration_source',
                   'activation_code_attempts')

    def __init__(self, *args, **kwargs):
        super(ProfileEditForm, self).__init__(*args, **kwargs)
        if 'privacy_policy' in self.fields:
            self.fields['privacy_policy'].required = True
        if 'phone' in self.fields:
            self.fields['phone'].required = True

    def require_fields(self, *fields):
        for field in fields:
            if not self.cleaned_data.get(field):
                self._errors[field] = self.error_class([self.fields[field].error_messages['required']])
                self.cleaned_data.pop(field, None)

    def clean(self):
        cd = super(ProfileEditForm, self).clean()

        if cd.get('type', UserProfile.INDIVIDUAL) == UserProfile.COMPANY:
            self.require_fields('business', 'nip')

        if cd.get('different_c_address', True):
            self.require_fields('c_city', 'c_street', 'c_local_number', 'c_zip_code', 'c_phone')

        return cd

    def save(self, *args, **kwargs):
        if self.instance.pk is None:
            site = Site.objects.get_current()
            self.instance.registration_source = site.domain
        return super(ProfileEditForm, self).save(*args, **kwargs)

    def send_email(self):
        profile = self.instance

        site = Site.objects.get_current()
        template_email(
            site.configuration.registration_email_recipient,
            _('User (%(email)s) has updated their profile on %(site)s') % {
                'email': profile.user.email,
                'site': site.name,
            },
            'auth_ex/profile_updated',
            kwargs={
                'profile': profile,
                'user': profile.user,
                'site': site,
            }
        )


class UserProfileNoSubscriberForm(ProfileEditForm):
    additional_data = forms.BooleanField(label=_('Additional data'), required=False)

    class Meta(ProfileEditForm.Meta):
        exclude = ('user', 'customer_id', 'card_id', 'privacy_policy', 'data_processing')


class UserProfileNoSubscriberPrivacyPolicyForm(UserProfileNoSubscriberForm):
    class Meta(ProfileEditForm.Meta):
        exclude = ('user', 'customer_id', 'card_id', 'data_processing')


class UserProfileForm(UserProfileNoSubscriberForm):
    is_subscriber = forms.BooleanField(label=_('I am a subscriber'), initial=False, required=False)

    def clean(self):
        cd = super(UserProfileForm, self).clean()

        if cd.get('is_subscriber'):
            if not cd.get('customer_id'):
                self._errors['customer_id'] = self.error_class([self.fields['customer_id'].error_messages['required']])
                cd.pop('customer_id', None)
            if not cd.get('card_id'):
                self._errors['card_id'] = self.error_class([self.fields['card_id'].error_messages['required']])
                cd.pop('card_id', None)

        return cd

    class Meta(ProfileEditForm.Meta):
        exclude = ('user', 'privacy_policy')


class CustomerDataForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('customer_id', 'card_id')

    def save(self, *args, **kwargs):
        profile = super(CustomerDataForm, self).save(*args, **kwargs)
        site = Site.objects.get_current()
        template_email(
            site.configuration.registration_email_recipient,
            _('User (%(email)s) has updated their profile on %(site)s') % {
                'email': profile.user.email,
                'site': site.name,
            },
            'auth_ex/customer_data_updated',
            kwargs={
                'profile': profile,
                'user': profile.user,
                'site': site,
            }
        )


# REGISTRATION


class CustomRegistrationFormTermsOfService(RegistrationFormTermsOfService):
    pass

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('password2')
        self.fields['email'].help_text = ''


class RegistrationFormTermsOfServiceUniqueEmail(
        CustomRegistrationFormTermsOfService, RegistrationFormUniqueEmail):
    pass


class ProfileRegistrationForm(FormContainer):
    user_form = RegistrationFormTermsOfServiceUniqueEmail
    profile_form = UserProfileForm
    goods_formset = GoodsFormSet
    good_category_formset = UserGoodCategoryFormSet

    def save(self, *args, **kwargs):
        site = Site.objects.get_current()
        activation = site.configuration.registration_requires_activation

        user = RegistrationProfile.objects.create_inactive_user(
            self.forms['user_form'],
            site, activation
        )

        if not activation:
            registration_profile = user.registrationprofile
            user.is_active = True
            user.save()
            registration_profile.activation_key = RegistrationProfile.ACTIVATED
            registration_profile.save()

        profile_form = self.forms['profile_form']
        profile_form.instance.user_id = user.pk
        profile_form.instance.pk = user.profile.pk
        profile_form.save()

        assert User.objects.filter(
            pk=user.pk, is_active=True, profile__pk=user.profile.pk).exists()

        return user


class ProfileRegistrationNoSubscriberForm(ProfileRegistrationForm):
    user_form = RegistrationFormTermsOfServiceUniqueEmail
    profile_form = UserProfileNoSubscriberForm
    goods_formset = GoodsFormSet
    good_category_formset = UserGoodCategoryFormSet
