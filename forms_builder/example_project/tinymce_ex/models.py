from django.contrib.admin import widgets as admin_widgets

from tinymce.models import HTMLField as OriginalHTMLField


class HTMLField(OriginalHTMLField):
    """
    A large string field for HTML content. It uses the TinyMCE widget in
    forms.
    """
    def formfield(self, **kwargs):
        from . import widgets as tinymce_widgets
        defaults = {'widget': tinymce_widgets.TinyMCE}
        defaults.update(kwargs)

        # As an ugly hack, we override the admin widget
        if defaults['widget'] == admin_widgets.AdminTextareaWidget:
            defaults['widget'] = tinymce_widgets.AdminTinyMCE

        return super().formfield(**defaults)
