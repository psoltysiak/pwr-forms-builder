from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.request import Request

from auth_ex.models import UserProfile
from utils import get_client_ip


class TermsUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = (
            'type',
            'first_name',
            'last_name',
            'birthday',
            'street',
            'local_number',
            'zip_code',
            'city',
            'business',
            'nip',
            'c_city',
            'c_local_number',
            'c_phone',
            'c_street',
            'c_zip_code',
        )


class TermsSerializer(serializers.Serializer):
    command = serializers.SerializerMethodField()
    email = serializers.EmailField(
        default=None
    )
    phone = serializers.CharField(
        default=None
    )
    description = serializers.CharField(
        default=None
    )
    marketing = serializers.CharField(
        default=None
    )
    personal = serializers.CharField(
        default=None
    )
    tele = serializers.CharField(
        default=None
    )
    ip = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        request = self._context.get('request')
        assert isinstance(request, Request), _(
            'Context have to contains the request'
        )

    def get_command(self, object):
        return self.context.get('command', None)

    def get_ip(self, object):
        request = self.context.get('request')
        return get_client_ip(request)
