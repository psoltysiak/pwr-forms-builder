from django import template

from ..views import get_perms_type


register = template.Library()


@register.inclusion_tag('terms/tags/terms.html')
def terms(description, style=None):
    """ Terms form.

        :param string description: Description from *kwargs.
    """

    return {
        'agreements': get_perms_type(description),
        'description': description,
        'style': style,
    }


@register.inclusion_tag('terms/tags/profile_terms.html')
def profile_terms(description):
    return terms(description)


@register.inclusion_tag('terms/tags/tap_terms.html')
def tap_terms(description):
    return terms(description)
